#!/usr/bin/env bash

set -eo pipefail


# OSX GUI apps do not pick up environment variables the same way as Terminal apps and there are no easy solutions,
# especially as Apple changes the GUI app behavior every release (see https://stackoverflow.com/q/135688/483528). As a
# workaround to allow GitHub Desktop to work, add this (hopefully harmless) setting here.
export PATH=$PATH:/usr/local/bin

if [ $# -eq 0 ]
then
    tfsec --config-file tfsec.yml .
elif [ $# -eq 1 ]
then
    tfsec --tfvars-file="$1"
else
    echo "Accept only 1 file <file_name>.tfvars as input!"
    exit 0
fi

exit $?
